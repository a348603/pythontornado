import tornado.web
import asyncio

class UsersHandler(tornado.web.RequestHandler):
    def post(self):
        self.write('Usuario creado')

    def get(self, user_id=None):
        if user_id is None:
            self.write('Lista de usuarios')
        else:
            self.write('Usuario con id: ' + user_id)

    def put(self, user_id):
        self.write('Usuario reemplazado con id: ' + user_id)

    def patch(self, user_id):
        self.write('Usuario actualizado con id: ' + user_id)

    def delete(self, user_id):
        self.write('Usuario eliminado con id: ' + user_id)

def make_app():
    return tornado.web.Application([
        (r'/users/', UsersHandler),
        (r'/users/(\d+)', UsersHandler),
    ])

async def main():
    app = make_app()
    app.listen(4499)
    await asyncio.Event().wait()

if __name__ == "__main__":
    asyncio.run(main())