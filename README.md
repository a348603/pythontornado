# Micro web frameworks

Web plataforms

## Unidad 1

Introducciòn al desarrollo web

## Lenguaje de programaciòn

Python3

## Autores

**Daniel Josue Lozano Porras**

  Usuario: Daniel3486
  
  Matrìcula: 348603

**Juan Antonio Dìaz Fernàndez**

  Usuario: JuanDiazuwu
  
  Matrìcula: 348637

**Jocelyn Soto Avila**

  Usuario: Jocelyn Soto Avila
  
  Matrìcula: 348687

## Instrucciones

Por parejas vamos a crear una aplicación (según nos haya tocado) hecha en:

* Python -> Tornado

* Java -> Spark

* Slim -> PHP

* Ruby -> Sinatra

* Scala -> Scalatra

Subir a repositorio de gitlab y enviar liga del repositorio.

## Funcionamiento

Este script es la implementación de una tabla restful, utilizando python y tornado.

## Requeriminetos

* 1 .- Python

```
sudo apt install python #python 2
```
```
sudo apt install python3 #python 3
```

* 2 .- pip

```
sudo apt install python-pip #python 2
```
```
sudo apt install python3-pip #python 3
```

* 3 .- tornado

```
pip install tornado #python 2
```
```
pip3 install tornado #python 3
```
